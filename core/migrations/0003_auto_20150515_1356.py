# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0002_auto_20150515_1313'),
    ]

    operations = [
        migrations.AlterField(
            model_name='uploadfile',
            name='fileobj',
            field=models.FileField(upload_to=b''),
        ),
    ]
