# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='UploadFile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('viewname', models.CharField(max_length=256)),
                ('filename', models.CharField(max_length=256)),
                ('fileobj', models.FileField(upload_to=b'media/')),
                ('hashsum', models.CharField(max_length=100)),
                ('length', models.IntegerField()),
                ('exif_camera', models.CharField(max_length=300, null=True, blank=True)),
                ('exif_created', models.DateField(null=True, blank=True)),
                ('uploaded', models.DateField(default=datetime.datetime.now)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
