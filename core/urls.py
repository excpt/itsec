from django.conf.urls import patterns, url
from django.conf.urls.static import static
from django.conf import settings

from core.views import FileDeleteView, FilesListView, FileUploadView, download_file

urlpatterns = patterns('',
    # Examples:
    url(r'^$', FilesListView.as_view(), name='home'),
    url(r'^download/(?P<pk>[0-9]+)/$', download_file, name='files.download'),
    url(r'^upload/$', FileUploadView.as_view(), name='files.upload'),
    url(r'^delete/(?P<pk>[0-9]+)/$', FileDeleteView.as_view(), name='files.delete'),
    # url(r'^blog/', include('blog.urls')),

    # url(r'^admin/', include(admin.site.urls)),
)

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_URL)