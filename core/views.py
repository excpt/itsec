# coding=utf-8
from hashlib import sha1
from datetime import datetime, timedelta

from django.core.files import File
from django.core.urlresolvers import reverse_lazy
from django.http import Http404, HttpResponse, HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.views.generic import ListView, DeleteView, CreateView
from django.core.servers.basehttp import FileWrapper
from PIL import Image

from core.forms import FileUploadForm
from core.models import UploadFile


class FilesListView(ListView):
    model = UploadFile
    template_name = 'files_list.html'


class FileDeleteView(DeleteView):
    model = UploadFile
    success_url = reverse_lazy('home')
    template_name = 'file_delete_confirm.html'


def download_file(request, pk):
    try:
        uploadfile = UploadFile.objects.filter(pk=pk)[0]
    except IndexError:
        raise Http404

    response = HttpResponse(FileWrapper(uploadfile.fileobj))
    response['Content-Disposition'] = 'attachment; filename=%s' % (
        uploadfile.filename,)
    return response


class FileUploadView(CreateView):
    form_class = FileUploadForm
    template_name = 'file_upload.html'
    success_url = reverse_lazy('home')

    def _make_thumb(self, formfile):
        thumb = Image.open(formfile)
        thumb.thumbnail((100, 100))
        thumb.save('media/thumbnails/thumb_%d.jpg' % (self.object.id,), 'JPEG')

    def form_valid(self, form):
        formfile = form.files['formfile']
        form.instance.filename = formfile.name
        file_content = formfile.read()

        try:
            im = Image.open(formfile)
        except IOError:
            context = {'msg': 'Неизвестный формат изображения.'}
            return render_to_response('upload_error.html',
                                      context,
                                      context_instance=RequestContext(self.request))
        hashsum = sha1(file_content).hexdigest()
        form.instance.hashsum = hashsum
        file_length = len(file_content)
        samefile = UploadFile.objects.filter(hashsum=hashsum,
                                      length=file_length).count()
        if samefile:
            context = {'msg': 'Файл уже загружен.'}
            return render_to_response('upload_error.html',
                                      context,
                                      context_instance=RequestContext(self.request))
        exif_data = im._getexif() or {}

        if not exif_data.get(0x9003):
            context = {'msg': 'Дата создания изображения не найдена.'}
            return render_to_response('upload_error.html',
                                      context,
                                      context_instance=RequestContext(self.request))

        form.instance.exif_model = exif_data.get(272)
        form.instance.exif_created = datetime.strptime(exif_data.get(0x9003),
                                                       '%Y:%m:%d %H:%M:%S')
        now = datetime.now()
        if now - timedelta(days=365) > form.instance.exif_created:
            context = {'msg': 'Фото создано более года назад.'}
            return render_to_response('upload_error.html',
                                      context,
                                      context_instance=RequestContext(self.request))
        form.instance.length = file_length
        self.object = form.save()
        self.object.fileobj.save('%s.file' % (self.object.id,),
                                 File(formfile))

        self._make_thumb(formfile)

        return HttpResponseRedirect(self.get_success_url())
