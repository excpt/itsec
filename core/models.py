from datetime import datetime
from django.db import models


class UploadFile(models.Model):
    viewname = models.CharField(max_length=256)
    filename = models.CharField(max_length=256)
    fileobj = models.FileField()
    hashsum = models.CharField(max_length=100)
    length = models.IntegerField()
    exif_model = models.CharField(max_length=100, null=True, blank=True)
    exif_created = models.DateField(null=True, blank=True)
    uploaded = models.DateField(default=datetime.now)

    def __unicode__(self):
        return self.viewname