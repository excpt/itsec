from django import forms

from core.models import UploadFile


class FileUploadForm(forms.ModelForm):
    class Meta:
        model = UploadFile
        fields = ('formfile', 'viewname')
    formfile = forms.FileField(allow_empty_file=False, required=True)